FROM node:14

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Copying rest of the application to app directory
COPY . /usr/src/app

# Expose the port and start the application
EXPOSE 8080

CMD ["npm", "start"]